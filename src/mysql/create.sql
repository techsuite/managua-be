CREATE TABLE Usuario(
    nombreDoctor VARCHAR(70) NOT NULL,
    nombreUsuario VARCHAR(70) PRIMARY KEY NOT NULL,
    password TEXT NOT NULL,
    dniDoctor VARCHAR(9) NOT NULL,
    apellidosDoctor varchar(70) NOT NULL
)

CREATE TABLE IF NOT EXISTS Paciente (
	dniPaciente VARCHAR(9) PRIMARY KEY NOT NULL,
	nombrePaciente VARCHAR(70) NOT NULL,
    apellidosPaciente VARCHAR(70) NOT NULL,
    gender VARCHAR(20) NOT NULL,
    birthday DATE NOT NULL,
    email VARCHAR(70) NOT NULL
)


CREATE TABLE managua.Prueba_pcr (
  id INT NOT NULL AUTO_INCREMENT,
  dniDoctor VARCHAR(45) DEFAULT NULL,
  dniPaciente VARCHAR(45) DEFAULT NULL,
  fechaFinal DATETIME DEFAULT NULL,
  fluorescencia INT DEFAULT NULL,
  PRIMARY KEY (id));


CREATE TABLE managua.Prueba_sangre (
  id INT NOT NULL AUTO_INCREMENT,
  dniDoctor VARCHAR(45) DEFAULT NULL,
  dniPaciente VARCHAR(45) DEFAULT NULL,
  fechaFinal DATETIME DEFAULT NULL,
  electrolitos INT DEFAULT NULL,
  glucosa INT DEFAULT NULL,
  colesterol INT DEFAULT NULL,
  trigliceridos INT DEFAULT NULL,
  bilirrubina INT DEFAULT NULL,
  PRIMARY KEY (id));