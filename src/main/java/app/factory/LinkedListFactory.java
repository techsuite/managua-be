package app.factory;


import org.springframework.stereotype.Component;

import java.util.LinkedList;

@Component
public class LinkedListFactory {

    public<T> LinkedList<T> newLinkedList(){
        return new LinkedList<T>();
    }
}
