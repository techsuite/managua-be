package app.factory;

import app.model.Prueba;
import app.model.response.InfoPage;
import app.model.response.ListPruebaPCRResponse;
import app.model.response.PruebaPCRResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PruebaPCRResponseFactory {

    public ListPruebaPCRResponse newListPruebaPCRResponse(List<PruebaPCRResponse> pruebas, InfoPage infoPage){
        return new ListPruebaPCRResponse(pruebas, infoPage);
    }
}