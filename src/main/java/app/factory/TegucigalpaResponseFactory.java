package app.factory;
import app.model.response.InfoPage;
import app.model.response.TegucigalpaResponse;
import app.model.response.TegucigalpaResponseWithMetadata;
import org.springframework.stereotype.Component;

@Component
public class TegucigalpaResponseFactory {
    public TegucigalpaResponseWithMetadata newTegucigalpaResponse(String employees, String patients, String bloodTest, String pcr, InfoPage infoPage){

        return new TegucigalpaResponseWithMetadata(new TegucigalpaResponse(employees,patients,bloodTest,pcr), infoPage);
    }

}
