package app.factory;

import app.model.Usuario;
import app.model.response.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UsuarioResponseFactory {

    public ListUsuarioResponse newUsuarioResponse(List<Usuario> usuario, InfoPage infoPage){
        return  new ListUsuarioResponse(usuario,infoPage);
    }
}
