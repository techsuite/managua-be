package app.factory;

import app.model.Prueba;
import app.model.response.InfoPage;
import app.model.response.ListPruebaSangreResponse;
import app.model.response.PruebaSangreResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PruebaSangreResponseFactory {

    public ListPruebaSangreResponse newListPruebaSangreResponse(List<PruebaSangreResponse> pruebas, InfoPage infoPage){
        return new ListPruebaSangreResponse(pruebas, infoPage);
    }
}
