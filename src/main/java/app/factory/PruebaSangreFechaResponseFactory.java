package app.factory;

import app.model.Prueba;
import app.model.response.InfoPage;
import app.model.response.ListPruebaSangreFechaResponse;
import app.model.response.PruebaSangreFechaResponse;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PruebaSangreFechaResponseFactory {

    public ListPruebaSangreFechaResponse newListPruebaSangreFechaResponse(List<PruebaSangreFechaResponse> pruebas, InfoPage infoPage){
        return new ListPruebaSangreFechaResponse(pruebas, infoPage);
    }
}