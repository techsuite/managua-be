package app.factory;

import app.model.Prueba;
import app.model.response.InfoPage;
import app.model.response.ListPruebaPCRFechaResponse;
import app.model.response.PruebaPCRFechaResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PruebaPCRFechaResponseFactory {

    public ListPruebaPCRFechaResponse newListPruebaPCRFechaResponse(List<PruebaPCRFechaResponse> pruebas, InfoPage infoPage){
        return new ListPruebaPCRFechaResponse(pruebas, infoPage);
    }
}