package app.factory;

import app.model.Paciente;
import app.model.response.ListPacienteResponse;
import app.model.response.InfoPage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PacienteResponseFactory {

    public ListPacienteResponse newPacienteResponse(List<Paciente> pacientes, InfoPage infoPage){
        return new ListPacienteResponse(pacientes, infoPage);
    }
}
