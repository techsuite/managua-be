package app.model;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class Usuario{
  private String nombreDoctor;
  private String apellidosDoctor;
  private String dniDoctor;
  private String nombreUsuario;
  private String password;
}