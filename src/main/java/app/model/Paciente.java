package app.model;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class Paciente{
  private String dniPaciente;
  private String nombrePaciente;
  private String apellidosPaciente;
  private String gender;
  private String birthday;
  private String email;
}