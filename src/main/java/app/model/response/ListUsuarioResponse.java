package app.model.response;

import java.util.List;

import app.model.Paciente;
import app.model.Usuario;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListUsuarioResponse {
    private List<Usuario> usuarios;
    private InfoPage infoPagina;
}
