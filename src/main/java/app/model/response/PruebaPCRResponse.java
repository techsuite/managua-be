package app.model.response;

import java.sql.Timestamp;
import java.util.List;

import app.model.Prueba;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PruebaPCRResponse {
    private String dniDoctor, dniPaciente;
    private int fluorescencia;
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Timestamp fechaFinal;
}