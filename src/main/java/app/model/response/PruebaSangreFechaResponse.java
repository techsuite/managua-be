package app.model.response;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import app.model.Prueba;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public
class PruebaSangreFechaResponse {
    private int id;
    private String dniDoctor, dniPaciente;
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Timestamp fechaFinal; 
    private Integer electrolitos, glucosa, colesterol, trigliceridos, bilirrubina;
}
