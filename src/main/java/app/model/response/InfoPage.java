package app.model.response;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class InfoPage{

  private Integer pageSize;
  private Integer pageNumber;
  private Integer totalPages;
}
