package app.model.response;

import java.util.List;

import app.model.Prueba;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListPruebaSangreResponse {
    private List<PruebaSangreResponse> pruebaSangre;
    private InfoPage infoPagina;
}
