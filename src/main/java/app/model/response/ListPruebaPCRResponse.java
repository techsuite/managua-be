package app.model.response;

import java.util.List;

import app.model.Prueba;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListPruebaPCRResponse {
    private List<PruebaPCRResponse> pruebaPCR;
    private InfoPage infoPagina;
}