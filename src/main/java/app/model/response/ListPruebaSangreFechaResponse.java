package app.model.response;

import java.util.List;

import app.model.Prueba;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListPruebaSangreFechaResponse {
    private List<PruebaSangreFechaResponse> pruebaSangre;
    private InfoPage infoPagina;
}

