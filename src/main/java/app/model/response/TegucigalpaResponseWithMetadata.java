package app.model.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TegucigalpaResponseWithMetadata {
        private TegucigalpaResponse data;
        private InfoPage infoPage;
}
