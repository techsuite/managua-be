package app.model.response;

import java.util.List;

import app.model.Prueba;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListPruebaPCRFechaResponse {
    private List<PruebaPCRFechaResponse> pruebaPCR;
    private InfoPage infoPagina;
}