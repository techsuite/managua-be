package app.model.response;

import java.util.List;

import app.model.Prueba;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TegucigalpaResponse {
    private String employees, patients,bloodTest,pcr;
}
