package app.model.response;

import java.util.List;

import app.model.Paciente;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListPacienteResponse {
    private List<Paciente> pacientes;
    private InfoPage infoPagina;
}
