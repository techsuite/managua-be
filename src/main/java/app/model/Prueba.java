package app.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class Prueba {
  private int id;
  private String dniDoctor;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Timestamp fechaFinal;     //Fecha y hora en Unix
  private String dniPaciente;
}
