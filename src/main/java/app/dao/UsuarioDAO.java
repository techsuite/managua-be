package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import app.model.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.connector.Connector;
import app.model.Usuario;

@Repository
public class UsuarioDAO{

	@Autowired
	private Connector connector;

	public Usuario getUsuario(String dni) throws SQLException{
		
		Usuario usuario = null;
		String query =
			"SELECT * FROM managua.Usuario WHERE dniDoctor=?";
		
		try (
		Connection connection = connector.getConnection();
		PreparedStatement pstmt = connection.prepareStatement(query);
		) {
			pstmt.setString(1, dni);

			try (ResultSet rs = pstmt.executeQuery()) {
				if (rs.next()) {
				usuario =
					new Usuario(
					rs.getString("nombreDoctor"),
					rs.getString("apellidosDoctor"),
					rs.getString("dniDoctor"),
					rs.getString("nombreUsuario"),
					rs.getString("password")
					);
				}
			}
		}
		return usuario;
	}

	public String empleadoListToString(List<Usuario> list){
		String res;
		if(!list.isEmpty()){
		res = "dniDoctor:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getDniDoctor()+",";
		}
		res +=list.get(list.size()-1).getDniDoctor()+";";
		res += "nombreDoctor:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getNombreDoctor()+",";
		}
		res +=list.get(list.size()-1).getNombreDoctor()+";";
		res += "apellidosDoctor:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getApellidosDoctor()+",";
		}
		res +=list.get(list.size()-1).getApellidosDoctor()+";";
		res += "password:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getPassword()+",";
		}
		res +=list.get(list.size()-1).getPassword()+";";
		res += "nombreUsuario:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getNombreUsuario()+",";
		}
		res +=list.get(list.size()-1).getNombreUsuario()+";";
		}else{
			res="dniDoctor:;nombreDoctor:;apellidosDoctor:;password:;nombreUsuario:;";
		}
		return res;

	}

}