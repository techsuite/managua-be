package app.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.connector.Connector;
import app.model.Paciente;

import java.util.*;

@Repository
public class PacienteDAO{

	@Autowired
	private Connector connector;

	public Paciente[] getPaciente(String dni) throws SQLException{
		
		Paciente[] resultados;
		List<Paciente> listaResultados = new ArrayList<Paciente>();
		Paciente paciente = null;
		String query =
			"SELECT * FROM managua.Paciente WHERE dniPaciente LIKE ?";
		
		try (
		Connection connection = connector.getConnection();
		PreparedStatement pstmt = connection.prepareStatement(query);
		) {
			pstmt.setString(1, dni+"%");

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
				paciente =
					new Paciente(
					rs.getString("dniPaciente"),
					rs.getString("nombrePaciente"),
					rs.getString("apellidosPaciente"),
					rs.getString("gender"),
					rs.getString("birthday"),
					rs.getString("email")
					);
				listaResultados.add(paciente);
				}
				resultados = new Paciente[listaResultados.size()];
				resultados = listaResultados.toArray(resultados);
			}
		}
		return resultados;
	}
	public Paciente[] getPacienteNombre(String nombre) throws SQLException{
		
		Paciente[] resultados;
		List<Paciente> listaResultados = new ArrayList<Paciente>(); 
		Paciente paciente = null;
		String query =
			"SELECT * FROM managua.Paciente WHERE nombrePaciente LIKE ?";
		
		try (
		Connection connection = connector.getConnection();
		PreparedStatement pstmt = connection.prepareStatement(query);
		) {
			pstmt.setString(1, nombre+"%");

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
				paciente =
					new Paciente(
					rs.getString("dniPaciente"),
					rs.getString("nombrePaciente"),
					rs.getString("apellidosPaciente"),
					rs.getString("gender"),
					rs.getString("birthday"),
					rs.getString("email")
					);
				listaResultados.add(paciente);
				}
				resultados = new Paciente[listaResultados.size()];
				resultados = listaResultados.toArray(resultados);
			}
		}
		return resultados;
	}
	public Paciente[] getPacienteApellidos(String apellidos) throws SQLException{
		
		Paciente[] resultados;
		List<Paciente> listaResultados = new ArrayList<Paciente>(); 
		Paciente paciente = null;
		String query =
			"SELECT * FROM managua.Paciente WHERE apellidosPaciente LIKE ?";
		
		try (
		Connection connection = connector.getConnection();
		PreparedStatement pstmt = connection.prepareStatement(query);
		) {
			pstmt.setString(1, "%"+apellidos+"%");

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
				paciente =
					new Paciente(
					rs.getString("dniPaciente"),
					rs.getString("nombrePaciente"),
					rs.getString("apellidosPaciente"),
					rs.getString("gender"),
					rs.getString("birthday"),
					rs.getString("email")
					);
				listaResultados.add(paciente);
				}
				resultados = new Paciente[listaResultados.size()];
				resultados = listaResultados.toArray(resultados);
			}
		}
		return resultados;
	}

	
    public void insertarPaciente(Paciente paciente) throws SQLException{
		String insertarPacienteSQL = "INSERT INTO Paciente VALUES (?, ?, ?, ?, ?, ?) ;";
        try(Connection conn = connector.getConnection();
          	PreparedStatement pst = conn.prepareStatement(insertarPacienteSQL);
          	){
          	//toLowerCase para asegurarnos de que es la misma pk en todos los casos
			  Date bday = null;
		  	pst.setString(1, paciente.getDniPaciente().toLowerCase());
		  	if(paciente.getBirthday() != null){
		  		bday = Date.valueOf(paciente.getBirthday());
			}
          	pst.setString(2, paciente.getNombrePaciente());
          	pst.setString(3, paciente.getApellidosPaciente());
          	pst.setDate(5, bday);
          	pst.setString(4, paciente.getGender());
		  	pst.setString(6, paciente.getEmail());

          	pst.executeUpdate();
          	pst.close();
        }
    }
    public String pacientesListToString(List<Paciente> list){
		String res;
		if(!list.isEmpty()) {
		res = "dniPaciente:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getDniPaciente()+",";
		}
		res +=list.get(list.size()-1).getDniPaciente()+";";
		res += "nombrePaciente:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getNombrePaciente()+",";
		}
		res +=list.get(list.size()-1).getNombrePaciente()+";";
		res += "apellidosPaciente:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getApellidosPaciente()+",";
		}
		res +=list.get(list.size()-1).getApellidosPaciente()+";";
		res += "birthday:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getBirthday()+",";
		}
		res +=list.get(list.size()-1).getBirthday()+";";
		res += "email:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getEmail()+",";
		}
		res +=list.get(list.size()-1).getEmail()+";";
		res += "gender:";
		for(int i = 0; i<list.size()-1; i++){
			res+=list.get(i).getGender()+",";
		}
		res +=list.get(list.size()-1).getGender()+";";
		}else{
		res="dniPaciente:;nombrePaciente:;apellidosPaciente:;birthday:;email:;gender:;";
		}
		return res;

	}

}