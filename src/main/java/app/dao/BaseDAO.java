package app.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import app.factory.LinkedListFactory;
import app.model.response.PruebaPCRResponse;
import app.model.response.PruebaSangreResponse;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import app.dao.connector.Connector;
import app.model.response.InfoPage;

@Repository
public class BaseDAO {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    @Autowired
    private LinkedListFactory linkedListFactory;

    protected String paginationQuery(InfoPage info) {
        return String.format(
        "LIMIT %d OFFSET %d",
        info.getPageSize(),
        (info.getPageSize() * (info.getPageNumber() - 1))
        );
    }

    public <T> List<T> getPageWithTipo(
        InfoPage info, 
        String tipoPrueba,
        Class<T>  tipo) throws Exception {
            String query = String.format("SELECT * FROM %s", tipoPrueba);
            return getPagesFromQuery(query, info, tipo);
        }

    public <T> List<T> getPagesFromQuery(
        String query,
        InfoPage info,
        Class<T> tipo
        ) throws Exception {
        String finalQuery = String.format("%s %s", query, paginationQuery(info));
        try (Connection conn = connector.getConnection()) {
          return queryRunner.query(
            conn,
            finalQuery,
            new BeanListHandler<>(
              tipo,
              // Comentario de Julio: esto hace que DBUtils ignore underscores y mayúsculas a la hora de mapear
              // las columnas con el bean (model). He visto que no funciona al completo pq en la base de datos tenéis
              // atributos de la forma "enfermedades_diag" y en el model "enfermedadesDiagnosticadas". Y claro,
              // no consigue mapear correctamente las columnas
              new BasicRowProcessor(new GenerousBeanProcessor())
            )
          );
        }
    }

    public Long numOfRowsFromQuery(String query) throws Exception {
        String finalQuery = String.format(
          "SELECT COUNT(*) FROM %s ;",
          query
        );
        try (Connection conn = connector.getConnection()) {
           return queryRunner.query(conn, finalQuery, new ScalarHandler<Long>());
        }
    }

    public Integer numOfRows(String tabla) throws Exception {
        return numOfRowsFromQuery(tabla).intValue();
    }

    public int numOfPages(Integer pageSize, Integer rowsInTable) throws Exception {
        if (rowsInTable % pageSize == 0)
            return rowsInTable / pageSize;
        else
            return (rowsInTable / pageSize) + 1;
    }

    public InfoPage getMetadataFromQuery(InfoPage info, String query) throws Exception {
        int numPages = numOfPages(info.getPageSize(), numOfRowsFromQuery(query).intValue());


        return info.getPageNumber() <= numPages
        ? new InfoPage(info.getPageSize(), info.getPageNumber(), numPages)
        : null;
    }

    // null if infoPage > totalPages
    public InfoPage getMetadata(InfoPage info,String tabla) throws Exception {
        return getMetadataFromQuery(info, tabla);
    }
    public<T> List<T> selectAllTable(String tabla, Class<T> type) throws Exception {
        String query = String.format(
                "SELECT * FROM %s ;",
                tabla
        );
        try (Connection conn = connector.getConnection()) {
            return queryRunner.query(conn, query, new BeanListHandler<>(type));
        }
    }


    public  List<Object> obtenerTodosLosAnalisis() throws Exception{
        LinkedList<Object> analisis = linkedListFactory.newLinkedList();
        List<PruebaPCRResponse> listpcr = selectAllTable("Prueba_pcr",PruebaPCRResponse.class);
        List<PruebaSangreResponse> listsangre = selectAllTable("Prueba_sangre",PruebaSangreResponse.class);
        analisis.addAll(listpcr);
        analisis.addAll(listsangre);

        return analisis;
    }

}
