package app.dao;


import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import app.model.Usuario;
import app.model.response.PruebaPCRResponse;
import app.model.response.PruebaSangreResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.connector.Connector;
import app.model.Prueba;

@Repository
public class PruebaDAO {
  
  @Autowired
	private Connector connector;

  public Prueba[] getPruebasEjecucion(String tabla) throws SQLException{
    Prueba[] res;
    ArrayList<Prueba> list = new ArrayList<Prueba>();

    String query = "SELECT * FROM managua."+tabla+
                   " WHERE fechaFinal > FROM_UNIXTIME("+Instant.now().getEpochSecond()+")";

    try(
      Connection connection = connector.getConnection();
      PreparedStatement pstmt = connection.prepareStatement(query);
    ){
      try(ResultSet rs = pstmt.executeQuery()){
        while(rs.next()){
          list.add(new Prueba(rs.getInt(1), rs.getString(2), rs.getTimestamp(4), rs.getString(3)));
        }
        
        res=new Prueba[list.size()];
        res=list.toArray(res);
      }
    }

    return res;
  }

  public Prueba[] getPruebasEspera(String tabla) throws SQLException{
    Prueba[] res;
    ArrayList<Prueba> list = new ArrayList<Prueba>();

    String query = "SELECT * FROM managua."+tabla+" WHERE fechaFinal IS NULL";

    try(
      Connection connection = connector.getConnection();
      PreparedStatement pstmt = connection.prepareStatement(query);
    ){
      try(ResultSet rs = pstmt.executeQuery()){
        while(rs.next()){
          list.add(new Prueba(rs.getInt(1), rs.getString(2), null, rs.getString(3)));
        }
        
        res=new Prueba[list.size()];
        res=list.toArray(res);
      }
    }

    return res;
  }

  public Prueba[] getPruebasPorActualizar(String tabla) throws SQLException{

    Prueba[] res;
    ArrayList<Prueba> list = new ArrayList<Prueba>();

    String query = "SELECT * FROM managua."+tabla+
                    " WHERE fechaFinal < FROM_UNIXTIME("+Instant.now().getEpochSecond()+") AND ";

    switch (tabla) {
      case "Prueba_pcr":
        query += "fluorescencia IS NULL" ;
        break;
      case "Prueba_sangre":
        query += "glucosa IS NULL";
        break;
      default:
        query = "";
        break;
    }

    try(
      Connection connection = connector.getConnection();
      PreparedStatement pstmt = connection.prepareStatement(query);
    ){
      try(ResultSet rs = pstmt.executeQuery()){
        while(rs.next()){
          list.add(new Prueba(rs.getInt(1), rs.getString(2), null, rs.getString(3)));
        }
        
        res=new Prueba[list.size()];
        res=list.toArray(res);
      }
    }

    return res;
  }

  public boolean updateDatePrueba(String tabla, int id) throws SQLException{
    
    boolean result = false;
    
    String query =  "UPDATE managua."+tabla+
                    " SET fechaFinal=FROM_UNIXTIME(?)"+
                    " WHERE id=?";

    try (
		  Connection connection = connector.getConnection();
		  PreparedStatement pstmt = connection.prepareStatement(query);
		) {
      pstmt.setLong(1, Instant.now().getEpochSecond() + 20);  // Actual + 20 s
      pstmt.setInt(2, id);
      int count = pstmt.executeUpdate();
      result = count > 0;
    }

    return result;
  }


	public boolean deletePrueba(int id, String tabla) throws SQLException{

    boolean result = false;
    
    String query =  "DELETE FROM managua."+tabla+" WHERE id = ?";
    try (
		  Connection connection = connector.getConnection();
		  PreparedStatement pstmt = connection.prepareStatement(query);
		) {
      pstmt.setInt(1, id);
      int count = pstmt.executeUpdate();
      result = count > 0;
    }

    return result;
  }
  
  // genera automaticamente los datos
  public boolean updatePrueba(String tabla, int id) throws SQLException{
    
    boolean result = false;
 
    String query =  "UPDATE managua."+tabla+" "+randomGenerator(tabla)+" WHERE id = ?";
    try (
		  Connection connection = connector.getConnection();
		  PreparedStatement pstmt = connection.prepareStatement(query);
		) {
      pstmt.setInt(1, id);
      int count = pstmt.executeUpdate();
      result = count > 0;
    }

    return result;
  }
  
  private String randomGenerator(String tabla){
    String set = "SET ";

    switch (tabla) {
      case "Prueba_pcr":
        set += "fluorescencia="+((int) (Math.random() * (10 - 0)));
        break;
      case "Prueba_sangre":
        set += "electrolitos="+((int) (Math.random() * (144 - 135)) + 135)+
               ", glucosa="+((int) (Math.random() * (110 - 70)) + 70)+
               ", colesterol="+((int) (Math.random() * (200 - 120)) + 120)+
               ", trigliceridos="+((int) (Math.random() * (280 - 30)) + 30)+
               ", bilirrubina="+((int) (Math.random() * (10 - 2)) + 2);
        break;
      default:
        set = "";
        break;
    }

    return set;
  }
  public int createPrueba(Prueba prueba, String tabla) throws SQLException{
    int pk = -1;
    String query = "INSERT INTO managua." + tabla + "(dniDoctor, dniPaciente) VALUES (?,?)";
    try(Connection conn = connector.getConnection();
    PreparedStatement pst = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    ){
      pst.setString(1, prueba.getDniDoctor());
      pst.setString(2, prueba.getDniPaciente());

      pst.executeUpdate();
      ResultSet rs = pst.getGeneratedKeys();
      if (rs.next()) pk = rs.getInt(1);
      pst.close();
    }
    return pk;
  }

  public String pcrListToString(List<PruebaPCRResponse> list){
    String res;
    if(!list.isEmpty()) {
       res = "dniPaciente:";
      for (int i = 0; i < list.size() - 1; i++) {
        res += list.get(i).getDniPaciente() + ",";
      }
      res += list.get(list.size() - 1).getDniPaciente() + ";";
      res += "dniDoctor:";
      for (int i = 0; i < list.size() - 1; i++) {
        res += list.get(i).getDniDoctor() + ",";
      }
      res += list.get(list.size() - 1).getDniDoctor() + ";";
      res += "fluorescencia:";
      for (int i = 0; i < list.size() - 1; i++) {
        res += list.get(i).getFluorescencia() + ",";
      }
      res += list.get(list.size() - 1).getFluorescencia() + ";";
      res += "fecha:";
      for (int i = 0; i < list.size() - 1; i++) {
        res += list.get(i).getFechaFinal().toLocalDateTime().toLocalDate().toString() + ",";
      }
      res += list.get(list.size() - 1).getFechaFinal().toLocalDateTime().toLocalDate().toString() + ";";

    }else{
      res="dniPaciente:;dniDoctor:;fluorescencia:;fecha:;";
    }
    return res;

  }
  public String pruebasDeSangreListToString(List<PruebaSangreResponse> list){
    String res;
    if(!list.isEmpty()) {
    res = "dniPaciente:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getDniPaciente()+",";
    }
    res +=list.get(list.size()-1).getDniPaciente()+";";
    res += "dniDoctor:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getDniDoctor()+",";
    }
    res +=list.get(list.size()-1).getDniDoctor()+";";
    res += "electrolitos:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getElectrolitos()+",";
    }
    res +=list.get(list.size()-1).getElectrolitos()+";";
    res += "glucosa:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getGlucosa()+",";
    }
    res +=list.get(list.size()-1).getGlucosa()+";";
    res += "colesterlol:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getColesterol()+",";
    }
    res +=list.get(list.size()-1).getColesterol()+";";
    res += "trigliceridos:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getTrigliceridos()+",";
    }
    res +=list.get(list.size()-1).getTrigliceridos()+";";
    res += "bilirrubina:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getBilirrubina()+",";
    }
    res +=list.get(list.size()-1).getBilirrubina()+";";
    res += "fecha:";
    for(int i = 0; i<list.size()-1; i++){
      res+=list.get(i).getFechaFinal().toLocalDateTime().toLocalDate().toString()+",";
    }
    res +=list.get(list.size()-1).getFechaFinal().toLocalDateTime().toLocalDate().toString()+";";

    }else{
      res="dniPaciente:;dniDoctor:;electrolitos:;glucosa:;colesterlol:;trigliceridos:;bilirrubina:;fecha:;";
    }
    return res;

  }


}
