package app.service;

import app.dao.PruebaDAO;
import app.model.Prueba;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PruebaService {

  @Autowired
  private PruebaDAO pruebaDAO;

  // Cada 10 segundos
  @Scheduled(cron = "*/10 * * * * *")
  public void updatePruebas() throws Exception {

    System.out.println("Actualizar datos");

    String[] servicios = {"Prueba_sangre", "Prueba_pcr"};
    Prueba[] ejecucion, cola, porActualizar;
    int contador;
    boolean hasUpdated;

    for (int i = 0; i < servicios.length; i++) {

      porActualizar = pruebaDAO.getPruebasPorActualizar(servicios[i]);
      //System.out.println("Por actualizar de "+servicios[i]+", dimensión: "+porActualizar.length);

      for (int j = 0; j < porActualizar.length; j++) {
        hasUpdated = pruebaDAO.updatePrueba(servicios[i], porActualizar[j].getId());
        //System.out.println("Ha actualizado el elemento por actualizar "+j+": "+hasUpdated);
      }


      ejecucion = pruebaDAO.getPruebasEjecucion(servicios[i]);
      //System.out.println("En ejecucion de "+servicios[i]+", dimensión: "+ejecucion.length);

      if(ejecucion.length < 3){
        cola = pruebaDAO.getPruebasEspera(servicios[i]);
        //System.out.println("En cola de "+servicios[i]+", dimensión: "+cola.length);

        contador = 3 - ejecucion.length;
        //System.out.println("Se pueden anadir a ejecutar: "+contador);

        for (int j = 0; j < cola.length && j < contador; j++) {
          hasUpdated = pruebaDAO.updateDatePrueba(servicios[i], cola[j].getId());
          //System.out.println("Ha actualizado el elemento de la cola "+j+": "+hasUpdated);
        }
      }
    }

  }
  
}
