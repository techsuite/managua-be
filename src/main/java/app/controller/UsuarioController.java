package app.controller;

import app.dao.BaseDAO;
import app.factory.UsuarioResponseFactory;
import app.model.Paciente;
import app.model.response.InfoPage;
import app.model.response.ListPacienteResponse;
import app.model.response.ListUsuarioResponse;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.dao.UsuarioDAO;
import app.model.Usuario;

import java.util.List;

@RestController
public class UsuarioController {

  @Autowired
  UsuarioDAO usuarioDAO;

  @Autowired
  BaseDAO baseDAO;

  @Autowired
  UsuarioResponseFactory usuarioResponseFactory;

  @GetMapping("usuario/{dni}/{password}")
  public ResponseEntity<Usuario> getUsuario(@PathVariable String dni, @PathVariable String password) 
    throws Exception {
      
    Usuario usuario = usuarioDAO.getUsuario(dni);
    boolean esOk = usuario != null ? usuario.getPassword().equals(password) : false;
    
    return esOk
    ? ResponseEntity.ok().body(usuario)
    : ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }

  @GetMapping("getEmployeesPaginacion/Pagina")
  public ResponseEntity <ListUsuarioResponse> getPruebasPaginacionSangre (@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
          throws Exception{

    InfoPage info = new InfoPage (pageSize, pageNumber, baseDAO.numOfPages(pageSize, baseDAO.numOfRows("Usuario")));

    List<Usuario> res = baseDAO.getPageWithTipo(info, "Usuario", Usuario.class);
    ListUsuarioResponse listUsuarioResponse = usuarioResponseFactory.newUsuarioResponse(res,info);
    return ResponseEntity.ok().body(listUsuarioResponse);
  }
}
