package app.controller;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

import app.dao.BaseDAO;
import app.dao.PacienteDAO;
import app.dao.UsuarioDAO;
import app.factory.LinkedListFactory;
import app.factory.PruebaPCRResponseFactory;
import app.factory.PruebaSangreResponseFactory;
import app.factory.TegucigalpaResponseFactory;
import app.model.Paciente;
import app.model.Usuario;
import app.model.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.web.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app.dao.PruebaDAO;
import app.model.Prueba;

@RestController
public class TegucigalpaController {

    @Autowired
    private BaseDAO baseDAO;

    @Autowired
    private PacienteDAO pacienteDAO;

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private PruebaDAO pruebaDAO;

    @Autowired
    private TegucigalpaResponseFactory tegucigalpaResponseFactory;

    @Autowired
    private LinkedListFactory linkedListFactory;


    @GetMapping("/analisis")
    public ResponseEntity<TegucigalpaResponseWithMetadata> getDatosForTeguci(@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
            throws Exception{

            List<Object> analisis = baseDAO.obtenerTodosLosAnalisis();
            List<PruebaSangreResponse> listsangre = linkedListFactory.newLinkedList();
            List<PruebaPCRResponse> listpcr = linkedListFactory.newLinkedList();
            List<Paciente> pacientes = linkedListFactory.newLinkedList();
            List<Usuario> usuarios = linkedListFactory.newLinkedList();
            //Primer elem : (pageSize * (pageNumber-1) )
            //Ultimo elem : (pageSize * (pageNumber-1) ) + pageSize

            InfoPage metadata = new InfoPage(pageSize,pageNumber, baseDAO.numOfPages(pageSize,analisis.size()));
            Boolean result = metadata.getPageNumber() <= metadata.getTotalPages();


            for (int i = (pageSize * (pageNumber-1) ); i< (pageSize * (pageNumber-1) ) + pageSize && result && i < analisis.size();i++){
                if(analisis.get(i) instanceof PruebaSangreResponse){

                    //Caso prueba_sangre
                    PruebaSangreResponse pruebaSangreResponse = (PruebaSangreResponse) analisis.get(i);
                    listsangre.add(pruebaSangreResponse);


                    Paciente[] paciente = pacienteDAO.getPaciente(pruebaSangreResponse.getDniPaciente());
                    Usuario doctor = usuarioDAO.getUsuario(pruebaSangreResponse.getDniDoctor());

                    for(int j=0; j<paciente.length; j++){
                        if(paciente[j]!=null && !pacientes.contains(paciente[j]))
                            pacientes.add(paciente[j]);
                    }
                    if(doctor!=null && !usuarios.contains(doctor))
                    usuarios.add(doctor);

                }else{

                    //Caso prueba_pcr
                    PruebaPCRResponse pruebaPCRResponse = (PruebaPCRResponse) analisis.get(i);
                    listpcr.add(pruebaPCRResponse);

                    Paciente[] paciente = pacienteDAO.getPaciente(pruebaPCRResponse.getDniPaciente());
                    Usuario doctor = usuarioDAO.getUsuario(pruebaPCRResponse.getDniDoctor());

                    for(int j=0; j<paciente.length; j++){
                        if(paciente[j]!=null && !pacientes.contains(paciente[j]))
                            pacientes.add(paciente[j]);
                    }
                    if(doctor!=null && !usuarios.contains(doctor))
                    usuarios.add(usuarioDAO.getUsuario(pruebaPCRResponse.getDniDoctor()));

                }
            }

            String employees = usuarioDAO.empleadoListToString(usuarios);
            String patients = pacienteDAO.pacientesListToString(pacientes);
            String bloodTest = pruebaDAO.pruebasDeSangreListToString(listsangre);
            String pcr = pruebaDAO.pcrListToString(listpcr);



            TegucigalpaResponseWithMetadata tegucigalpaResponse = tegucigalpaResponseFactory.newTegucigalpaResponse(employees,patients,bloodTest,pcr, metadata);

            return result
                    ? ResponseEntity.ok().body(tegucigalpaResponse)
                    : ResponseEntity.badRequest().build();


    }
}
