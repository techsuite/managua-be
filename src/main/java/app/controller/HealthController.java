package app.controller;

import app.model.response.HealthResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

  @GetMapping("/status")
  public ResponseEntity<HealthResponse> alive() {
    HealthResponse healthResponse = new HealthResponse("alive v1");

    return ResponseEntity.ok().body(healthResponse);
  }
}
