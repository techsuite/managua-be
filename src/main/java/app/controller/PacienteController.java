package app.controller;

import java.sql.SQLException;
import java.util.List;

import app.dao.BaseDAO;
import app.factory.PacienteResponseFactory;
import app.model.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app.dao.PacienteDAO;
import app.model.Paciente;

@RestController
public class PacienteController {

  @Autowired
  PacienteDAO pacienteDAO;

  @Autowired
  BaseDAO baseDAO;

  @Autowired
  PacienteResponseFactory pacienteResponseFactory;

    @GetMapping("paciente/{dni}")
  public ResponseEntity<Paciente[]> getPaciente(@PathVariable String dni) 
    throws Exception {
      
    Paciente[] resultados = pacienteDAO.getPaciente(dni);
    boolean esOk = resultados.length != 0;
    
    return esOk
    ? ResponseEntity.ok().body(resultados)
    : ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }
  @GetMapping("nombre/{nombre}")
  public ResponseEntity<Paciente[]> getPacienteNombre(@PathVariable String nombre) 
    throws Exception {
      Paciente[] resultados = pacienteDAO.getPacienteNombre(nombre);
    boolean esOk = resultados.length != 0;
    
    return esOk
    ? ResponseEntity.ok().body(resultados)
    : ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }
  @GetMapping("apellidos/{apellidos}")
  public ResponseEntity<Paciente[]> getPacienteApellidos(@PathVariable String apellidos) 
    throws Exception {
      Paciente[] resultados = pacienteDAO.getPacienteApellidos(apellidos);
    boolean esOk = resultados.length != 0;
    
    return esOk
    ? ResponseEntity.ok().body(resultados)
    : ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }

  @PostMapping("/nuevoPaciente")
    public void insertarPaciente(@RequestBody Paciente paciente) throws SQLException {
        pacienteDAO.insertarPaciente(paciente);
    }

  @GetMapping("getPacientesPaginacion/Pagina")
  public ResponseEntity <ListPacienteResponse> getPruebasPaginacionSangre (@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
          throws Exception{

    InfoPage info = new InfoPage (pageSize, pageNumber, baseDAO.numOfPages(pageSize, baseDAO.numOfRows("Paciente")));

    List<Paciente> res = baseDAO.getPageWithTipo(info, "Paciente", Paciente.class);
    ListPacienteResponse listPacienteResponse = pacienteResponseFactory.newPacienteResponse(res,info);
    return ResponseEntity.ok().body(listPacienteResponse);
  }

}
