package app.controller;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import app.dao.BaseDAO;
import app.factory.PruebaPCRFechaResponseFactory;
import app.factory.PruebaPCRResponseFactory;
import app.factory.PruebaSangreFechaResponseFactory;
import app.factory.PruebaSangreResponseFactory;
import app.model.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app.dao.PruebaDAO;
import app.model.Prueba;

@RestController
public class PruebaController {
  
  @Autowired
  PruebaDAO pruebaDAO;

  @Autowired
  BaseDAO baseDAO;

  @Autowired
  private PruebaPCRResponseFactory pruebaPCRResponseFactory;

  @Autowired
  private PruebaSangreResponseFactory pruebaSangreResponseFactory;

  @Autowired
  private PruebaPCRFechaResponseFactory pruebaPCRFechaResponseFactory;

  @Autowired
  private PruebaSangreFechaResponseFactory pruebaSangreFechaResponseFactory;

  @GetMapping("getPruebasEjecucion/{tipoPrueba}")
  public ResponseEntity<Prueba[]> getPruebasEjecucion(@PathVariable String tipoPrueba)
  throws Exception{

    Prueba[] res = pruebaDAO.getPruebasEjecucion(tipoPrueba);
    long now = Instant.now().toEpochMilli();


    for (int i = 0; i < res.length; i++) {
      res[i].setFechaFinal(Timestamp.from(Instant.ofEpochMilli(res[i].getFechaFinal().getTime() - now)));
    }

    return ResponseEntity.ok().body(res);
  }

  @GetMapping("getPruebasEspera/{tipoPrueba}")
  public ResponseEntity<Prueba[]> getPruebasEspera(@PathVariable String tipoPrueba)
  throws Exception{

    Prueba[] res = pruebaDAO.getPruebasEspera(tipoPrueba);

    return ResponseEntity.ok().body(res);
  }

  @GetMapping("getPruebasPorActualizar/{tipoPrueba}")
  public ResponseEntity <Prueba[]> getPruebasPorActualizar (@PathVariable String tipoPrueba)
  throws Exception{

    Prueba[] res = pruebaDAO.getPruebasPorActualizar(tipoPrueba);

    return ResponseEntity.ok().body(res);
  }

  
  @GetMapping("getPruebasPaginacion/Prueba_sangre/Pagina")
  public ResponseEntity <ListPruebaSangreResponse> getPruebasPaginacionSangre (@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
  throws Exception{

    InfoPage info = new InfoPage (pageSize, pageNumber, baseDAO.numOfPages(pageSize, baseDAO.numOfRows("Prueba_sangre")));

    List<PruebaSangreResponse> res = baseDAO.getPageWithTipo(info, "Prueba_sangre", PruebaSangreResponse.class);
    ListPruebaSangreResponse listPruebaSangreResponse = pruebaSangreResponseFactory.newListPruebaSangreResponse(res,info);
    return ResponseEntity.ok().body(listPruebaSangreResponse);
  }

  @GetMapping("getPruebasPaginacion/Prueba_pcr/Pagina")
  public ResponseEntity <ListPruebaPCRResponse> getPruebasPaginacionPCR (@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
  throws Exception{

    InfoPage info = new InfoPage (pageSize, pageNumber, baseDAO.numOfPages(pageSize, baseDAO.numOfRows("Prueba_pcr")));

    List<PruebaPCRResponse> res = baseDAO.getPageWithTipo(info, "Prueba_pcr", PruebaPCRResponse.class);
    ListPruebaPCRResponse listPruebaPCRResponse = pruebaPCRResponseFactory.newListPruebaPCRResponse(res,info);
    return ResponseEntity.ok().body(listPruebaPCRResponse);
  }

  @GetMapping("getPruebasFechaPaginacion/Prueba_pcr/Pagina")
  public ResponseEntity <ListPruebaPCRFechaResponse> getPruebasFechaPaginacionPCR (@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
  throws Exception{

    InfoPage info = new InfoPage (pageSize, pageNumber, baseDAO.numOfPages(pageSize, baseDAO.numOfRows("Prueba_pcr")));

    List<PruebaPCRFechaResponse> res = baseDAO.getPageWithTipo(info, "Prueba_pcr", PruebaPCRFechaResponse.class);
    ListPruebaPCRFechaResponse listPruebaPCRFechaResponse = pruebaPCRFechaResponseFactory.newListPruebaPCRFechaResponse(res,info);

    return ResponseEntity.ok().body(listPruebaPCRFechaResponse);
  }
  @GetMapping("getPruebasFechaPaginacion/Prueba_sangre/Pagina")
  public ResponseEntity <ListPruebaSangreFechaResponse> getPruebasFechaPaginacionSangre (@RequestParam Integer pageNumber, @RequestParam Integer pageSize)
  throws Exception{

    InfoPage info = new InfoPage (pageSize, pageNumber, baseDAO.numOfPages(pageSize, baseDAO.numOfRows("Prueba_sangre")));

    List<PruebaSangreFechaResponse> res = baseDAO.getPageWithTipo(info, "Prueba_sangre", PruebaSangreFechaResponse.class);
    ListPruebaSangreFechaResponse listPruebaSangreFechaResponse = pruebaSangreFechaResponseFactory.newListPruebaSangreFechaResponse(res,info);
    return ResponseEntity.ok().body(listPruebaSangreFechaResponse);
  }

  @PatchMapping("updateDatePrueba/{tipoPrueba}/{id}")
  public ResponseEntity<Prueba> updateDatePrueba(@PathVariable String tipoPrueba, @PathVariable int id) 
  throws Exception {
    
  boolean hasUpdated = pruebaDAO.updateDatePrueba(tipoPrueba, id);

  return hasUpdated
  ? ResponseEntity.ok().build()
  : ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
}


  @DeleteMapping("prueba/{tipoPrueba}/{id}")
  public ResponseEntity<Prueba> deletePrueba(@PathVariable String tipoPrueba, @PathVariable int id) 
    throws Exception {
      
    boolean haEliminado = pruebaDAO.deletePrueba(id, tipoPrueba);

    return haEliminado
    ? ResponseEntity.ok().build()
    : ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }


  @PatchMapping("updatePrueba/{tipoPrueba}/{id}")
  public ResponseEntity<Prueba> updatePrueba(@PathVariable String tipoPrueba, @PathVariable int id) 
    throws Exception {

    boolean hasUpdated = pruebaDAO.updatePrueba(tipoPrueba, id);

    return hasUpdated
    ? ResponseEntity.ok().build()
    : ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
  }

  @PostMapping("/addPruebaPCR")
    public ResponseEntity<Void> nuevaPruebaPCR(@RequestBody Prueba pcr) throws Exception {
        pruebaDAO.createPrueba(pcr, "Prueba_pcr");
        return ResponseEntity.ok().build();
    }

    @PostMapping("/addPruebaSangre")
    public ResponseEntity<Void> nuevaPruebaSangre(@RequestBody Prueba analysis) throws Exception {
        pruebaDAO.createPrueba(analysis, "Prueba_sangre");
        return ResponseEntity.ok().build();
    }
}
