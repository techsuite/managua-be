package app.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import app.factory.LinkedListFactory;
import app.model.Usuario;
import app.model.response.InfoPage;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Connection;

import app.model.response.PruebaPCRResponse;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

//Con las demás tablas que no sean Usuario da problemas
@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseDaoTest extends GenericDaoTest {

  @Autowired
  private BaseDAO baseDAO;

  @MockBean
  private LinkedListFactory linkedListFactory;

  @Mock
  private LinkedList<Object> analisis;

  @Test
  public void numOfRows_whenCalledAndSuccess()
    throws Exception {
    when(queryRunner.query(eq(connection), anyString(), any()))
      .thenReturn(new Long(1));
    int res = baseDAO.numOfRows("dummyTabla");
    assertThat(res == 1).isTrue();
  }

  @Test
  public void numOfPages_whenCalledAndSuccess()
    throws Exception {
    long res = baseDAO.numOfPages(2, 5);
    assertThat(res == 3).isTrue();
    res = baseDAO.numOfPages(2, 6);
    assertThat(res == 3).isTrue();
  }

  @Test(expected = NullPointerException.class)
  public void getPagesFromQuery_whenCalledAndNoArgument()
    throws Exception {
    baseDAO.getPagesFromQuery("", null, Usuario.class);
  }


  @Test
  public void getPagesFromQuery_whenCalledAndSuccess()
    throws Exception {
    List<Usuario> ls = new ArrayList<Usuario>();
    ls.add(null);
    ls.add(null);
    when(queryRunner.query(any(Connection.class), anyString(), any()))
      .thenReturn(ls);
    baseDAO.getPagesFromQuery("", new InfoPage(1,1,1), Usuario.class);
  
    assertThat(ls.size() == 2).isTrue();
  }

  @Test
  public void obtenerTodosLosAnalisis() throws Exception{
    when(linkedListFactory.newLinkedList()).thenReturn(analisis);
    baseDAO.obtenerTodosLosAnalisis();

    verify(queryRunner,times(2)).query(eq(connection),anyString(),any());
  }
  @Test
  public void getPageWithTipo_when_called_go_correctly() throws Exception{
    InfoPage info = new InfoPage(1,1,1);
    baseDAO.getPageWithTipo(info,"dummyPrueba",PruebaPCRResponse.class);

    verify(queryRunner).query(eq(connection),anyString(),any());
  }

  @Test
  public void getMetadataFromQuery_when_called_go_correctly() throws Exception{
    InfoPage info = new InfoPage(1,1,1);
    when(queryRunner.query(eq(connection),anyString(),any())).thenReturn(Long.valueOf(1));
    InfoPage res = baseDAO.getMetadataFromQuery(info,"dummyQuery");

    assertThat(info).isEqualTo(res);
  }

  @Test
  public void getMetadataFromQuery_when_called_wrong_return_null() throws Exception{
    InfoPage info = new InfoPage(1,2,1);
    when(queryRunner.query(eq(connection),anyString(),any())).thenReturn(Long.valueOf(1));
    InfoPage res = baseDAO.getMetadataFromQuery(info,"dummyQuery");

    assertThat(res).isEqualTo(null);
  }

  @Test
  public void getMetadata_when_called_go_correctly() throws Exception{
    InfoPage info = new InfoPage(1,1,1);
    when(queryRunner.query(eq(connection),anyString(),any())).thenReturn(Long.valueOf(1));
    InfoPage res = baseDAO.getMetadata(info, "dummyTabla");

    assertThat(res).isEqualTo(info);
  }
}

