package app.dao;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import app.model.Paciente;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PacienteDaoTest extends GenericDaoTest{

  @Autowired
  private PacienteDAO pacienteDAO;

  @Test
  public void getPaciente_poneParamCorrectos() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    pacienteDAO.getPaciente("11111111W");
    
    verify(preparedStatement).setString(1, "11111111W%");
  }   
  
  @Test(expected = SQLException.class)
  public void getPaciente_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    pacienteDAO.getPaciente("11111111W");
  }

  @Test
  public void getPaciente_siNoHayResultado_devuelveNull()
    throws Exception {
    when(resultSet.next()).thenReturn(false);

    Paciente[] ouput = pacienteDAO.getPaciente("11111111W");

    assertNotEquals(Collections.EMPTY_LIST, ouput);
  }
  
  @Test
  public void getPacienteNombre_poneParamCorrectos() throws Exception {
    
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    pacienteDAO.getPacienteNombre("David");
    verify(preparedStatement).setString(1, "David%");
  }  

  
  @Test(expected = SQLException.class)
  public void getPacienteNombre_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    pacienteDAO.getPacienteNombre("11111111W");
  }

  @Test
  public void getPacienteNombre_siNoHayResultado_devuelveNull()
    throws Exception {
    when(resultSet.next()).thenReturn(false);

    Paciente [] ouput = pacienteDAO.getPacienteNombre("11111111W");

    assertNotEquals(Collections.EMPTY_LIST, ouput);
  }

  @Test
  public void getPacienteApellidos_poneParamCorrectos() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    Paciente [] output =pacienteDAO.getPacienteApellidos("Vinas Viruta");
    assertNotEquals(new Paciente[0],output);
  }

  @Test
  public void getPacienteApellidos_when_resultSet_is_Empty() throws Exception {
    when(resultSet.next()).thenReturn(false);
    Paciente [] output =pacienteDAO.getPacienteApellidos("Vinas Viruta");
    assertThat(output).isEqualTo(new Paciente[0]);
  }
  
  @Test(expected = SQLException.class)
  public void getPacienteApellidos_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    pacienteDAO.getPacienteApellidos("11111111W");
  }



  @Test(expected = SQLException.class)
  public void insertarPaciente_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt() throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    Paciente p = new Paciente();
    p.setDniPaciente("12345678A");
    
    pacienteDAO.insertarPaciente(p);
  }


  @Test(expected = NullPointerException.class)
  public void insertarPaciente_whenNoTieneDNI_throwException() throws Exception {
    Paciente p = new Paciente();

    pacienteDAO.insertarPaciente(p);
  }

  @Test
  public void insertarPaciente_whenCallAndResult() throws Exception {
    Paciente p = new Paciente();
    p.setDniPaciente("12345678A");
    p.setBirthday("2010-10-10");

    pacienteDAO.insertarPaciente(p);
  }

  @Test
  public void insertarPaciente_when_bday_is_null() throws Exception {
    Paciente p = new Paciente();
    p.setDniPaciente("12345678A");

    pacienteDAO.insertarPaciente(p);
  }

  @Test
  public void pacientesListToStringNoElem() throws Exception{

    List<Paciente> listPacientes = List.of();
    String res = "dniPaciente:;nombrePaciente:;apellidosPaciente:;birthday:;email:;gender:;";
    String output = pacienteDAO.pacientesListToString(listPacientes);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pacientesListToStringOneElem() throws Exception{
    Paciente p = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender","birthday","email");
    List<Paciente> listPacientes = List.of(p);
    String res = "dniPaciente:dniPaciente;nombrePaciente:nombrePaciente;apellidosPaciente:apellidosPaciente;birthday:birthday;email:email;gender:gender;";
    String output = pacienteDAO.pacientesListToString(listPacientes);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pacientesListToStringTwoElems() throws Exception{
    Paciente p = new Paciente("dniPaciente1","nombrePaciente1","apellidosPaciente1","gender1","birthday1","email1");
    Paciente p2 = new Paciente("dniPaciente2","nombrePaciente2","apellidosPaciente2","gender2","birthday2","email2");
    List<Paciente> listPacientes = List.of(p,p2);
    String res = "dniPaciente:dniPaciente1,dniPaciente2;nombrePaciente:nombrePaciente1,nombrePaciente2;apellidosPaciente:apellidosPaciente1,apellidosPaciente2" +
            ";birthday:birthday1,birthday2;email:email1,email2;gender:gender1,gender2;";
    String output = pacienteDAO.pacientesListToString(listPacientes);
    assertThat(output).isEqualTo(res);
  }

}
