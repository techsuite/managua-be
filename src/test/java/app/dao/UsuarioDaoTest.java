package app.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.list;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.List;

import app.model.Paciente;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import app.model.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioDaoTest extends GenericDaoTest{

  @Autowired
  private UsuarioDAO usuarioDAO;

  @Test
  public void getUsuario_poneParamCorrectos() throws Exception {
    when(resultSet.next()).thenReturn(true);

    usuarioDAO.getUsuario("dummy");

    verify(preparedStatement).setString(1, "dummy");
  }   
  
  @Test(expected = SQLException.class)
  public void getUsuario_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    usuarioDAO.getUsuario("dummy");
  }

  @Test
  public void getUsuario_siNoHayResultado_devuelveNull()
    throws Exception {
    when(resultSet.next()).thenReturn(false);

    Usuario ouput = usuarioDAO.getUsuario("dummy");

    assertNull(ouput);
  }
  @Test
  public void usuariosListToStringNoElem() throws Exception{

    List<Usuario> listUsuario = List.of();
    String res = "dniDoctor:;nombreDoctor:;apellidosDoctor:;password:;nombreUsuario:;";
    String output = usuarioDAO.empleadoListToString(listUsuario);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void usuariosListToStringOneElem() throws Exception{
    Usuario p = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario","password");
    List<Usuario> listUsuarios = List.of(p);
    String res = "dniDoctor:dniDoctor;nombreDoctor:nombreDoctor;apellidosDoctor:apellidosDoctor;password:password;nombreUsuario:nombreUsuario;";
    String output = usuarioDAO.empleadoListToString(listUsuarios);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pacientesListToStringTwoElems() throws Exception{
    Usuario p = new Usuario("nombreDoctor1","apellidosDoctor1","dniDoctor1","nombreUsuario1","password1");
    Usuario p2 = new Usuario("nombreDoctor2","apellidosDoctor2","dniDoctor2","nombreUsuario2","password2");
    List<Usuario> listUsuario = List.of(p,p2);
    String res = "dniDoctor:dniDoctor1,dniDoctor2;nombreDoctor:nombreDoctor1,nombreDoctor2;apellidosDoctor:apellidosDoctor1,apellidosDoctor2;password:password1,password2" +
            ";nombreUsuario:nombreUsuario1,nombreUsuario2;";
    String output = usuarioDAO.empleadoListToString(listUsuario);
    assertThat(output).isEqualTo(res);
  }
}
