package app.dao;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import app.model.Usuario;
import app.model.response.PruebaPCRResponse;
import app.model.response.PruebaSangreResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import app.model.Prueba;



@RunWith(SpringRunner.class)
@SpringBootTest
public class PruebaDAOTest extends GenericDaoTest{

  @Autowired
  private PruebaDAO pruebaDAO;

  @Test(expected = SQLException.class)
  public void getPruebasEjecucion_lanzaCorrectamenteExcepcion() 
  throws Exception{
      when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

      pruebaDAO.getPruebasEjecucion("Prueba_pcr");
  }

  @Test
  public void getPruebasEjecucion_go_correctly()
          throws Exception{
    when(resultSet.next()).thenReturn(true).thenReturn(false);

    Prueba[] res=pruebaDAO.getPruebasEjecucion("Prueba_pcr");
    assertNotEquals(new Prueba[0],res);
  }

  @Test
  public void getPruebasEjecucion_siNoHayResultado_devuelveArrayVacio() throws Exception{
    when(resultSet.next()).thenReturn(false);

    Prueba[] output = pruebaDAO.getPruebasEjecucion("Prueba_pcr");

    assertArrayEquals(new Prueba[0], output);
  }

  @Test(expected = SQLException.class)
  public void getPruebasEspera_lanzaCorrectamenteExcepcion() 
  throws Exception{
      when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

      pruebaDAO.getPruebasEspera("Prueba_pcr");
  }

  @Test
  public void getPruebasEspera_go_correctly()
          throws Exception{
    when(resultSet.next()).thenReturn(true).thenReturn(false);

    Prueba[] res=pruebaDAO.getPruebasEspera("Prueba_pcr");
    assertNotEquals(new Prueba[0],res);
  }

  @Test
  public void getPruebasEspera_siNoHayResultado_devuelveArrayVacio() throws Exception{
    when(resultSet.next()).thenReturn(false);

    Prueba[] output = pruebaDAO.getPruebasEspera("Prueba_pcr");

    assertArrayEquals(new Prueba[0], output);
  }


  @Test(expected = SQLException.class)
  public void getPruebasPorActualizar_lanzaCorrectamenteExcepcion() 
  throws Exception{
      when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

      pruebaDAO.getPruebasPorActualizar("Prueba_pcr");
  }

  @Test
  public void getPruebasPorActualizar_go_correctly()
          throws Exception{
    when(resultSet.next()).thenReturn(true).thenReturn(false);

    Prueba[] output = pruebaDAO.getPruebasPorActualizar("Prueba_pcr");
    assertNotEquals(new Prueba[0], output);
  }

  @Test
  public void getPruebasPorActualizar_with_not_table()
          throws Exception{
    when(resultSet.next()).thenReturn(true).thenReturn(false);

    Prueba[] output = pruebaDAO.getPruebasPorActualizar("");
    assertNotEquals(new Prueba[0],output);
  }

  @Test
  public void getPruebasPorActualizar_siNoHayResultado_devuelveArrayVacio() throws Exception{
    when(resultSet.next()).thenReturn(false);

    Prueba[] output = pruebaDAO.getPruebasPorActualizar("Prueba_pcr");

    assertArrayEquals(new Prueba[0], output);
  }

  @Test(expected = SQLException.class)
  public void updateDatePrueba_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    pruebaDAO.updateDatePrueba("Prueba_pcr",1);
  }

  @Test
  public void updateDatePrueba_siHayPrueba_devuelveFalse()
          throws Exception {
    when(preparedStatement.executeUpdate()).thenReturn(0);

    boolean ouput = pruebaDAO.updateDatePrueba("Prueba_sangre",1);
    assertFalse(ouput);
  }

  @Test
  public void updateDatePrueba_siHayPrueba_devuelveTrue()
    throws Exception {
    when(preparedStatement.executeUpdate()).thenReturn(1);

    boolean ouput = pruebaDAO.updateDatePrueba("Prueba_pcr",1);

    assertTrue(ouput);
  }


  @Test(expected = SQLException.class)
  public void deletePrueba_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    pruebaDAO.deletePrueba(1, "Usuario");
  }
  
  @Test
  public void deletePrueba_siNoHayPrueba_devuelveFalse()
    throws Exception {
    when(preparedStatement.executeUpdate()).thenReturn(0);

    boolean ouput = pruebaDAO.deletePrueba(1, "Usuario");

    assertFalse(ouput);
  }


  @Test
  public void deletePrueba_siHayPrueba_devuelveTrue()
    throws Exception {
    when(preparedStatement.executeUpdate()).thenReturn(1);

    boolean ouput = pruebaDAO.deletePrueba(1, "Usuario");

    assertTrue(ouput);
  }

  @Test(expected = SQLException.class)
  public void updatePrueba_lanzaCorrectamenteExcepcion()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    pruebaDAO.updatePrueba("Prueba_sangre",1);
  }

  @Test
  public void updatePrueba_siNoHayPrueba_devuelveFalse()
    throws Exception {
    when(preparedStatement.executeUpdate()).thenReturn(0);

    boolean ouput = pruebaDAO.updatePrueba("Prueba_pcr",1);

    assertFalse(ouput);
  }


  @Test
  public void updatePrueba_siHayPrueba_devuelveTrue()
    throws Exception {
    when(preparedStatement.executeUpdate()).thenReturn(1);

    boolean ouput = pruebaDAO.updatePrueba("dummytabla",1);


    assertTrue(ouput);
  }

  @Test
  public void insertarPrueba_whenCalledIsntInserted() throws  SQLException {
    Prueba c = new Prueba();
    c.setDniPaciente("a");
    c.setDniDoctor("b");

    when(connection.prepareStatement(anyString(), anyInt()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), any());
    doNothing().when(preparedStatement).setDate(anyInt(), any());
    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();
    when(preparedStatement.getGeneratedKeys()).thenReturn(resultSet);
    when(resultSet.next()).thenReturn(false);
    Integer pk = pruebaDAO.createPrueba(c, "Tabla_pcr");
    Assert.isTrue(pk == -1);
  }

  @Test
  public void insertarPrueba_whenCalledIsInserted() throws Exception {
    Prueba prueba = new Prueba();
    prueba.setDniPaciente("a");
    prueba.setDniDoctor("b");

    when(connection.prepareStatement(anyString(), anyInt()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), any());
    doNothing().when(preparedStatement).setDate(anyInt(), any());
    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();
    when(preparedStatement.getGeneratedKeys()).thenReturn(resultSet);
    when(resultSet.next()).thenReturn(true);
    when(resultSet.getInt(anyInt())).thenReturn(20);
    Integer pk = pruebaDAO.createPrueba(prueba, null);
    Assert.isTrue(pk == 20);
  }
  @Test
  public void pruebaPCRListToStringNoElem() throws Exception{

    List<PruebaPCRResponse> listpcr = List.of();
    String res = "dniPaciente:;dniDoctor:;fluorescencia:;fecha:;";
    String output = pruebaDAO.pcrListToString(listpcr);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pruebaPCRListToStringOneElem() throws Exception{
    PruebaPCRResponse p = new PruebaPCRResponse("dniDoctor","dniPaciente",0, new Timestamp(System.currentTimeMillis()));
    List<PruebaPCRResponse> listpcr = List.of(p);
    String res = "dniPaciente:dniPaciente;dniDoctor:dniDoctor;fluorescencia:0;fecha:"+new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate().toString()+";";
    String output = pruebaDAO.pcrListToString(listpcr);
    assertThat(output).isEqualTo(res);
  }

  @Test
  public void pruebaPCRListToStringTwoElems() throws Exception{
    PruebaPCRResponse p = new PruebaPCRResponse("dniDoctor1","dniPaciente1",0, new Timestamp(System.currentTimeMillis()));
    PruebaPCRResponse p2 = new PruebaPCRResponse("dniDoctor2","dniPaciente2",0, new Timestamp(System.currentTimeMillis()));
    List<PruebaPCRResponse> listpcr = List.of(p,p2);
    String res = "dniPaciente:dniPaciente1,dniPaciente2;dniDoctor:dniDoctor1,dniDoctor2;fluorescencia:0,0;fecha:"+new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate().toString()+","+new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate().toString()+";";
    String output = pruebaDAO.pcrListToString(listpcr);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pruebaSangreListToStringNoElem() throws Exception{

    List<PruebaSangreResponse> listsng = List.of();
    String res = "dniPaciente:;dniDoctor:;electrolitos:;glucosa:;colesterlol:;trigliceridos:;bilirrubina:;fecha:;";
    String output = pruebaDAO.pruebasDeSangreListToString(listsng);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pruebaSangreListToStringOneElem() throws Exception{
    PruebaSangreResponse p = new PruebaSangreResponse("dniDoctor","dniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
    List<PruebaSangreResponse> listsng = List.of(p);
    String res = "dniPaciente:dniPaciente;dniDoctor:dniDoctor;electrolitos:0;glucosa:0;colesterlol:0;trigliceridos:0;bilirrubina:0;fecha:"+new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate().toString()+";";
    String output = pruebaDAO.pruebasDeSangreListToString(listsng);
    assertThat(output).isEqualTo(res);
  }
  @Test
  public void pruebaSangreListToStringTwoElems() throws Exception{
    PruebaSangreResponse p = new PruebaSangreResponse("dniDoctor1","dniPaciente1",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
    PruebaSangreResponse p2 = new PruebaSangreResponse("dniDoctor2","dniPaciente2",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
    List<PruebaSangreResponse> listsng = List.of(p,p2);
    String res = "dniPaciente:dniPaciente1,dniPaciente2;dniDoctor:dniDoctor1,dniDoctor2;electrolitos:0,0;glucosa:0,0;colesterlol:0,0;trigliceridos:0,0;bilirrubina:0,0;fecha:"+new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate().toString()+","+new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate().toString()+";";
    String output = pruebaDAO.pruebasDeSangreListToString(listsng);
    assertThat(output).isEqualTo(res);
  }
}