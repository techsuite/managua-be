package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.time.Instant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import app.dao.PruebaDAO;
import app.model.Prueba;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PruebaServiceTest {
  
  @Autowired
  private PruebaService pruebaService;

  @MockBean
  private PruebaDAO pruebaDAO;


  @Test
  public void updatePruebas_alLlamar_debeEjecutarMetodosActualizarPruebas()
    throws Exception {
    
    Prueba[] pruebasEjecucion = {
      new Prueba(1, "pepe2", Timestamp.from(Instant.now()), "cliente2")
    };

    Prueba[] pruebasEspera = {
      new Prueba(2, "pepe1", null, "cliente1"),
      new Prueba(3, "pepe2", null, "cliente2"),
      new Prueba(4, "pepe3", null, "cliente3"),
      new Prueba(5, "pepe4", null, "cliente4")
    };

    Prueba[] pruebasActualizar = { new Prueba(1, "pepe1", null, "cliente1")};

    when(pruebaDAO.getPruebasPorActualizar(anyString())).thenReturn(pruebasActualizar);
    when(pruebaDAO.getPruebasEjecucion(anyString())).thenReturn(pruebasEjecucion);
    when(pruebaDAO.getPruebasEspera(anyString())).thenReturn(pruebasEspera);

    pruebaService.updatePruebas();

    verify(pruebaDAO, times(2)).updateDatePrueba(anyString(), eq(2));
    verify(pruebaDAO, times(2)).updateDatePrueba(anyString(), eq(3));
  }
  @Test
  public void updatePruebas_with_More_than_3_in_ejecucion() throws Exception {
    Prueba[] pruebasEjecucion = {
            new Prueba(1, "dummyDni1", Timestamp.from(Instant.now()), "dummyDniPac1"),
            new Prueba(2, "dummyDni2", Timestamp.from(Instant.now()), "dummyDniPac2"),
            new Prueba(3, "dummyDni3", Timestamp.from(Instant.now()), "dummyDniPac3"),
            new Prueba(4, "dummyDni4", Timestamp.from(Instant.now()), "dummyDniPac4")
    };

    when(pruebaDAO.getPruebasPorActualizar(anyString())).thenReturn(new Prueba[0]);
    when(pruebaDAO.getPruebasEjecucion(anyString())).thenReturn(pruebasEjecucion);
    when(pruebaDAO.getPruebasEspera(anyString())).thenReturn(new Prueba[0]);

    pruebaService.updatePruebas();
  }
  @Test
  public void updatePruebas_when_cola_is_empty() throws Exception {
    Prueba[] pruebasEjecucion = {
            new Prueba(1, "dummyDni1", Timestamp.from(Instant.now()), "dummyDniPac1"),
            new Prueba(2, "dummyDni2", Timestamp.from(Instant.now()), "dummyDniPac2"),
    };

    when(pruebaDAO.getPruebasPorActualizar(anyString())).thenReturn(new Prueba[0]);
    when(pruebaDAO.getPruebasEjecucion(anyString())).thenReturn(pruebasEjecucion);
    when(pruebaDAO.getPruebasEspera(anyString())).thenReturn(new Prueba[0]);

    pruebaService.updatePruebas();
  }
  //cola_is_Empty
}
