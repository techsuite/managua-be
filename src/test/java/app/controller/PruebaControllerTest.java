package app.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.sql.Timestamp;
import java.util.List;

import static org.mockito.Mockito.when;

import app.dao.BaseDAO;
import app.factory.PruebaPCRFechaResponseFactory;
import app.factory.PruebaPCRResponseFactory;
import app.factory.PruebaSangreFechaResponseFactory;
import app.factory.PruebaSangreResponseFactory;
import app.model.Paciente;
import app.model.response.*;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import app.dao.PruebaDAO;
import app.model.Prueba;



@RunWith(SpringRunner.class)
@WebMvcTest(PruebaController.class)
public class PruebaControllerTest {
  
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PruebaDAO pruebaoDAO;

  @MockBean
  private BaseDAO baseDAO;

  @MockBean
  private PruebaPCRResponseFactory pruebaPCRResponseFactory;

  @MockBean
  private PruebaSangreResponseFactory pruebaSangreResponseFactory;
  
  @MockBean
  private PruebaPCRFechaResponseFactory pruebaPCRFechaResponseFactory;

  @MockBean
  private PruebaSangreFechaResponseFactory pruebaSangreFechaResponseFactory;

  @Test
  public void getPruebasEjecucion_noHayPruebas_devuelveVacio() throws Exception{
    Prueba[] res = new Prueba[0];
    when(pruebaoDAO.getPruebasEjecucion(anyString())).thenReturn(res);

    mockMvc.perform(get("/getPruebasEjecucion/Prueba_pcr"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(0)));
  }

  @Test
  public void getPruebasEjecucion_hayPruebas_devuelveOk() throws Exception{
    Prueba[] prueba = {new Prueba (1, "pepe", Timestamp.valueOf("2021-04-06 17:17:15"),"juan")};
    when(pruebaoDAO.getPruebasEjecucion(anyString())).thenReturn(prueba);

    mockMvc
    .perform(get("/getPruebasEjecucion/Prueba_pcr"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(1)))
    .andExpect(jsonPath("$.[0].*", hasSize(4)))
    .andExpect(jsonPath("$.[0].id", is(prueba[0].getId())))
    .andExpect(jsonPath("$.[0].dniDoctor", is(prueba[0].getDniDoctor())))
    // No compruebo la fecha porque depende del momento en el que se haga la consulta
    .andExpect(jsonPath("$.[0].dniPaciente", is(prueba[0].getDniPaciente())));
  }

  @Test
  public void getPruebasEspera_noHayPruebas_devuelveVacio() throws Exception{
    Prueba[] res = new Prueba[0];
    when(pruebaoDAO.getPruebasEspera(anyString())).thenReturn(res);

    mockMvc.perform(get("/getPruebasEspera/Prueba_pcr"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(0)));
  }

  @Test
  public void getPruebasEspera_hayPruebas_devuelveOk() throws Exception{
    Prueba[] prueba = {new Prueba (1, "pepe", null,"juan")};
    when(pruebaoDAO.getPruebasEspera(anyString())).thenReturn(prueba);

    mockMvc
    .perform(get("/getPruebasEspera/Prueba_pcr"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(1)))
    .andExpect(jsonPath("$.[0].*", hasSize(4)))
    .andExpect(jsonPath("$.[0].id", is(prueba[0].getId())))
    .andExpect(jsonPath("$.[0].dniDoctor", is(prueba[0].getDniDoctor())))
    .andExpect(jsonPath("$.[0].fechaFinal", is(prueba[0].getFechaFinal())))
    .andExpect(jsonPath("$.[0].dniPaciente", is(prueba[0].getDniPaciente())));
  }

  @Test
  public void getPruebasPorActualizar_noHayPruebas_devuelveVacio() throws Exception{
    Prueba[] res = new Prueba[0];
    when(pruebaoDAO.getPruebasPorActualizar(anyString())).thenReturn(res);

    mockMvc.perform(get("/getPruebasPorActualizar/Prueba_pcr"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(0)));
  }

  @Test
  public void getPruebasPorActualizar_hayPruebas_devuelveOk() throws Exception{
    Prueba[] prueba = {new Prueba (1, "pepe", null,"juan")};
    when(pruebaoDAO.getPruebasPorActualizar(anyString())).thenReturn(prueba);

    mockMvc
    .perform(get("/getPruebasPorActualizar/Prueba_pcr"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(1)))
    .andExpect(jsonPath("$.[0].*", hasSize(4)))
    .andExpect(jsonPath("$.[0].id", is(prueba[0].getId())))
    .andExpect(jsonPath("$.[0].dniDoctor", is(prueba[0].getDniDoctor())))
    .andExpect(jsonPath("$.[0].fechaFinal", is(prueba[0].getFechaFinal())))
    .andExpect(jsonPath("$.[0].dniPaciente", is(prueba[0].getDniPaciente())));
  }



  @Test
  public void updateDatePrueba_llamarConPruebaInexistente_devuelveNotModified()
    throws Exception {
    when(pruebaoDAO.updateDatePrueba(anyString(), anyInt())).thenReturn(false);

    mockMvc.perform(patch("/updateDatePrueba/Prueba_pcr/98")).andExpect(status().isNotModified());
  }

  @Test
  public void updateDatePrueba_llamarConPruebaExistente_devuelveOK()
    throws Exception {
    when(pruebaoDAO.updateDatePrueba(anyString(), anyInt())).thenReturn(true);

    mockMvc.perform(patch("/updateDatePrueba/Prueba_pcr/8")).andExpect(status().isOk());
  }



  @Test
  public void deletePrueba_llamarConPruebaInexistente_devuelveNoContent()
    throws Exception {
    when(pruebaoDAO.deletePrueba(anyInt(), anyString())).thenReturn(false);

    mockMvc.perform(delete("/prueba/Prueba_pcr/97")).andExpect(status().isNoContent());
  }

  @Test
  public void deletePrueba_llamarConPruebaExistente_devuelveOK()
    throws Exception {
    when(pruebaoDAO.deletePrueba(anyInt(), anyString())).thenReturn(true);

    mockMvc.perform(delete("/prueba/Prueba_pcr/1")).andExpect(status().isOk());
  }


  @Test
  public void updatePrueba_llamarConPruebaInexistente_devuelveNotModified()
    throws Exception {
    when(pruebaoDAO.updatePrueba(anyString(), anyInt())).thenReturn(false);

    mockMvc.perform(patch("/updatePrueba/Prueba_pcr/8")).andExpect(status().isNotModified());
  }

  @Test
  public void updatePrueba_llamarConPruebaExistente_devuelveOK()
    throws Exception {
    when(pruebaoDAO.updatePrueba(anyString(), anyInt())).thenReturn(true);

    mockMvc.perform(patch("/updatePrueba/Prueba_sangre/6")).andExpect(status().isOk());
  }

  @Test
  public void addPruebaPCR_whenWorks() throws Exception {
    
    mockMvc.perform(post("/addPruebaPCR").content(new JSONObject(pruebaoDAO).toString())
    .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk());
    }

  @Test
  public void addPruebaSangre_whenWorks() throws Exception {

    mockMvc.perform(post("/addPruebaSangre")
      .content(new JSONObject(pruebaoDAO).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk());
  }

  @Test
  public void getPruebasPaginacionSangre_hayPruebas_devuelveOk() throws Exception{
    PruebaSangreResponse pruebaSangreResponse =new PruebaSangreResponse("dummyDniDoctor","dummyDniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
    InfoPage info = new InfoPage (1, 1, 1);
    List<PruebaSangreResponse> prueba = List.of(pruebaSangreResponse);
    ListPruebaSangreResponse listPruebaSangreResponse   = new ListPruebaSangreResponse(prueba,info);

    when(baseDAO.getPageWithTipo(info,"DummyTabla",PruebaSangreResponse.class)).thenReturn(prueba);
    when(pruebaSangreResponseFactory.newListPruebaSangreResponse(any(),any())).thenReturn(listPruebaSangreResponse);

    mockMvc
            .perform(get("/getPruebasPaginacion/Prueba_sangre/Pagina?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.pruebaSangre", hasSize(1)))
            .andExpect(jsonPath("$.infoPagina.totalPages",is(1)));
  }

  @Test
  public void getPruebasPaginacionPCR_hayPruebas_devuelveOk() throws Exception{
    PruebaPCRResponse pruebaPCRResponse =new PruebaPCRResponse("dummyDniDoctor","dummyDniPaciente",0, new Timestamp(System.currentTimeMillis()));
    InfoPage info = new InfoPage (1, 1, 1);
    List<PruebaPCRResponse> prueba = List.of(pruebaPCRResponse);
    ListPruebaPCRResponse listPruebaPCRResponse   = new ListPruebaPCRResponse(prueba,info);

    when(baseDAO.getPageWithTipo(info,"DummyTabla",PruebaPCRResponse.class)).thenReturn(prueba);
    when(pruebaPCRResponseFactory.newListPruebaPCRResponse(any(),any())).thenReturn(listPruebaPCRResponse);

    mockMvc
            .perform(get("/getPruebasPaginacion/Prueba_pcr/Pagina?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.pruebaPCR", hasSize(1)))
            .andExpect(jsonPath("$.infoPagina.totalPages",is(1)));
  }

  @Test
  public void getPruebasFechaPaginacionPCR_hayPruebas_devuelveOk() throws Exception{
    PruebaPCRFechaResponse pruebaPCRFechaResponse =new PruebaPCRFechaResponse(0, "dummyDniDoctor","dummyDniPaciente",null, 0);
    InfoPage info = new InfoPage (1, 1, 1);
    List<PruebaPCRFechaResponse> prueba = List.of(pruebaPCRFechaResponse);
    ListPruebaPCRFechaResponse listPruebaPCRFechaResponse   = new ListPruebaPCRFechaResponse(prueba,info);

    when(baseDAO.getPageWithTipo(info,"DummyTabla",PruebaPCRFechaResponse.class)).thenReturn(prueba);
    when(pruebaPCRFechaResponseFactory.newListPruebaPCRFechaResponse(any(),any())).thenReturn(listPruebaPCRFechaResponse);

    mockMvc
            .perform(get("/getPruebasFechaPaginacion/Prueba_pcr/Pagina?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.pruebaPCR", hasSize(1)))
            .andExpect(jsonPath("$.infoPagina.totalPages",is(1)));
  }

  @Test
  public void getPruebasFechaPaginacionSangre_hayPruebas_devuelveOk() throws Exception{
    PruebaSangreFechaResponse pruebaSangreFechaResponse =new PruebaSangreFechaResponse(0, "dummyDniDoctor","dummyDniPaciente",null, 0, 0, 0, 0, 0);
    InfoPage info = new InfoPage (1, 1, 1);
    List<PruebaSangreFechaResponse> prueba = List.of(pruebaSangreFechaResponse);
    ListPruebaSangreFechaResponse listPruebaSangreFechaResponse   = new ListPruebaSangreFechaResponse(prueba,info);

    when(baseDAO.getPageWithTipo(info,"DummyTabla",PruebaSangreFechaResponse.class)).thenReturn(prueba);
    when(pruebaSangreFechaResponseFactory.newListPruebaSangreFechaResponse(any(),any())).thenReturn(listPruebaSangreFechaResponse);

    mockMvc
            .perform(get("/getPruebasFechaPaginacion/Prueba_sangre/Pagina?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.pruebaSangre", hasSize(1)))
            .andExpect(jsonPath("$.infoPagina.totalPages",is(1)));
  }
  }
