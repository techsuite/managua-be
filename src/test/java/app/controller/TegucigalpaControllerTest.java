
package app.controller;

import app.controller.TegucigalpaController;
import app.dao.BaseDAO;
import app.dao.PacienteDAO;
import app.dao.PruebaDAO;
import app.dao.UsuarioDAO;
import app.factory.LinkedListFactory;
import app.factory.TegucigalpaResponseFactory;
import app.model.Paciente;
import app.model.Prueba;
import app.model.Usuario;
import app.model.response.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(TegucigalpaController.class)
public class TegucigalpaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BaseDAO baseDAO;

    @MockBean
    private PacienteDAO pacienteDAO;

    @MockBean
    private UsuarioDAO usuarioDAO;

    @MockBean
    private PruebaDAO pruebaDAO;

    @MockBean
    private TegucigalpaResponseFactory tegucigalpaResponseFactory;

    @MockBean
    private LinkedListFactory linkedListFactory;


    @Test
    public void getTegucigalpa_worksWithBlood() throws Exception{
        PruebaSangreResponse pruebaSangreResponse = new PruebaSangreResponse("dniDoctor",
                "dniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaSangreResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");

        InfoPage metadata = new InfoPage(1,1,1);
        TegucigalpaResponse tegucigalpaResponse = new TegucigalpaResponse("dummy","dummy","dummy","dummy");
        TegucigalpaResponseWithMetadata tegucigalpaResponseWithMetadata = new TegucigalpaResponseWithMetadata(tegucigalpaResponse,metadata);
        when(tegucigalpaResponseFactory.newTegucigalpaResponse(anyString(),anyString(),anyString(),anyString(),any())).thenReturn(tegucigalpaResponseWithMetadata);

        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
        ;
    }
    @Test
    public void getTegucigalpa_worksWithPCR() throws Exception{
        PruebaPCRResponse pruebaPCRResponse = new PruebaPCRResponse("dniDoctor",
                "dniPaciente",0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaPCRResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");

        InfoPage metadata = new InfoPage(1,1,1);
        TegucigalpaResponse tegucigalpaResponse = new TegucigalpaResponse("dummy","dummy","dummy","dummy");
        TegucigalpaResponseWithMetadata tegucigalpaResponseWithMetadata = new TegucigalpaResponseWithMetadata(tegucigalpaResponse,metadata);
        when(tegucigalpaResponseFactory.newTegucigalpaResponse(anyString(),anyString(),anyString(),anyString(),any())).thenReturn(tegucigalpaResponseWithMetadata);

        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }
    @Test
    public void getTegucigalpa_dontworksWithPCR() throws Exception{
        PruebaPCRResponse pruebaPCRResponse = new PruebaPCRResponse("dniDoctor",
                "dniPaciente",0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente",
                "apellidosPaciente","gender", "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor",
                "dniDoctor","nombreUsuario", "password");
        List<Object> analisis = List.of(pruebaPCRResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(0);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getTegucigalpa_with_PCR_when_paciente_is_notnull_but_pacientes_contains_paciente() throws Exception{
        PruebaPCRResponse pruebaPCRResponse = new PruebaPCRResponse("dniDoctor",
                "dniPaciente",0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaPCRResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        pacientes.add(paciente);
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }

    @Test
    public void getTegucigalpa_with_PCR_when_paciente_is_null_and_pacientes_not_contains_paciente() throws Exception{
        PruebaPCRResponse pruebaPCRResponse = new PruebaPCRResponse("dniDoctor",
                "dniPaciente",0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = new Paciente[0];
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaPCRResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        pacientes.add(paciente);
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }

    @Test
    public void getTegucigalpa_with_PCR_when_doctor__is_notnull_but_doctores_contains_doctor() throws Exception{
        PruebaPCRResponse pruebaPCRResponse = new PruebaPCRResponse("dniDoctor",
                "dniPaciente",0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaPCRResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        usuarios.add(usuario);
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }
    @Test
    public void getTegucigalpa_with_PCR_when_doctor_is_null_and_doctores_contains_doctor() throws Exception{
        PruebaPCRResponse pruebaPCRResponse = new PruebaPCRResponse("dniDoctor",
                "dniPaciente",0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaPCRResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        usuarios.add(usuario);
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(null);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }
    @Test
    public void getTegucigalpa_with_PruebaSangre_when_paciente_is_notnull_but_pacientes_contains_paciente() throws Exception{
        PruebaSangreResponse pruebaSangreResponse = new PruebaSangreResponse("dniDoctor",
                "dniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente",
                "apellidosPaciente","gender", "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor",
                "dniDoctor","nombreUsuario", "password");
        List<Object> analisis = List.of(pruebaSangreResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        pacientes.add(paciente);
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }

    @Test
    public void getTegucigalpa_with_PruebaSangre_when_paciente_is_null_and_pacientes_not_contains_paciente() throws Exception{
        PruebaSangreResponse pruebaSangreResponse = new PruebaSangreResponse("dniDoctor",
                "dniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaSangreResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        pacientes.add(paciente);
        LinkedList<Object> usuarios = new LinkedList<>();
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }

    @Test
    public void getTegucigalpa_with_PruebaSangre_when_doctor__is_notnull_but_doctores_contains_doctor() throws Exception{
        PruebaSangreResponse pruebaSangreResponse = new PruebaSangreResponse("dniDoctor",
                "dniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaSangreResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        usuarios.add(usuario);
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(usuario);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }
    @Test
    public void getTegucigalpa_with_PruebaSangre_when_doctor_is_null_and_doctores_contains_doctor() throws Exception{
        PruebaSangreResponse pruebaSangreResponse = new PruebaSangreResponse("dniDoctor",
                "dniPaciente",0,0,0,0,0, new Timestamp(System.currentTimeMillis()));
        Paciente paciente = new Paciente("dniPaciente","nombrePaciente","apellidosPaciente","gender",
                "birthday","email");
        Paciente[] resultados = {paciente};
        Usuario usuario = new Usuario("nombreDoctor","apellidosDoctor","dniDoctor","nombreUsuario",
                "password");
        List<Object> analisis = List.of(pruebaSangreResponse);
        LinkedList<Object> listsangre = new LinkedList<>();
        LinkedList<Object> listpcr = new LinkedList<>();
        LinkedList<Object> pacientes = new LinkedList<>();
        LinkedList<Object> usuarios = new LinkedList<>();
        usuarios.add(usuario);
        when(baseDAO.obtenerTodosLosAnalisis()).thenReturn(analisis);
        when(baseDAO.numOfPages(any(),any())).thenReturn(1);
        when(linkedListFactory.newLinkedList()).thenReturn(listsangre).thenReturn(listpcr)
                .thenReturn(pacientes).thenReturn(usuarios);
        when(pacienteDAO.getPaciente("dniPaciente")).thenReturn(resultados);
        when(usuarioDAO.getUsuario("dniDoctor")).thenReturn(null);

        when(usuarioDAO.empleadoListToString(any())).thenReturn("dummy");
        when(pacienteDAO.pacientesListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pruebasDeSangreListToString(any())).thenReturn("dummy");
        when(pruebaDAO.pcrListToString(any())).thenReturn("dummy");
        mockMvc
                .perform(get("/analisis?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk());
    }
}