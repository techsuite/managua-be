package app.controller;

import app.dao.BaseDAO;
import app.factory.PacienteResponseFactory;
import app.model.response.InfoPage;
import app.model.response.ListPacienteResponse;
import org.json.JSONObject;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.any;

import app.dao.PacienteDAO;
import app.model.Paciente;

import java.util.List;


@RunWith(SpringRunner.class)
@WebMvcTest(PacienteController.class)
public class PacienteControllerTest {
  
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PacienteDAO pacienteDAO;

  @MockBean
  BaseDAO baseDAO;

  @MockBean
  PacienteResponseFactory pacienteResponseFactory;

  @Test
  public void getPaciente_llamarConDNIExistente_goWell()
    throws Exception {
      Paciente paciente = new Paciente("11111111W", "David","Vinas Viruta", "Hombre", "2010-02-04", "dajamo32@hotmail.com");
      Paciente[] resultados = {paciente};
      when(pacienteDAO.getPaciente(anyString())).thenReturn(resultados);
  
      mockMvc.perform(get("/paciente/11111111W")).andExpect(status().isOk());
    }
  
    public void getPaciente_llamarConPacienteInexistente_devuelveUnauthorized()
      throws Exception {
        Paciente[] resultados = new Paciente[0];
        when(pacienteDAO.getPaciente(anyString())).thenReturn(resultados);

        mockMvc.perform(get("/paciente/11111111W")).andExpect(status().isUnauthorized());
    }


  @Test
  public void getPacienteNombre_llamarConPacienteNombreInexistente_devuelveUnauthorized()
    throws Exception {
    Paciente paciente = new Paciente("11111111W", "David","Vinas Viruta", "Hombre", "2010-02-04", "dajamo32@hotmail.com");
    Paciente[] resultados = {paciente};
    when(pacienteDAO.getPacienteNombre(anyString())).thenReturn(resultados);

    mockMvc.perform(get("/nombre/Fran")).andExpect(status().isOk());
  }

  @Test
  public void getPacienteNombre_llamarConPacienteNombreExistente_go_well()
          throws Exception {
    Paciente[] resultados = new Paciente[0];
    when(pacienteDAO.getPacienteNombre(anyString())).thenReturn(resultados);

    mockMvc.perform(get("/nombre/Fran")).andExpect(status().isUnauthorized());
  }


  @Test
  public void getPacienteApellidos_llamarConPacienteApellidosInexistente_devuelveUnauthorized()
    throws Exception {
    Paciente paciente = new Paciente("11111111W", "David","Vinas Viruta", "Hombre", "2010-02-04", "dajamo32@hotmail.com");
    Paciente[] resultados = {paciente};
    when(pacienteDAO.getPacienteApellidos(anyString())).thenReturn(resultados);

    mockMvc.perform(get("/apellidos/Perez Gomez")).andExpect(status().isOk());
  }

  @Test
  public void getPacienteApellidos_llamarConPacienteApellidosExistente_go_well()
          throws Exception {
    Paciente[] resultados = new Paciente[0];
    when(pacienteDAO.getPacienteApellidos(anyString())).thenReturn(resultados);

    mockMvc.perform(get("/apellidos/Perez Gomez")).andExpect(status().isUnauthorized());
  }


  /*@Test
  public void getPacienteNombre_llamarConPacienteExistente_devuelveOk()
    throws Exception {
    Paciente paciente = new Paciente("11111111W", "David","Vinas Viruta", "Hombre", "2010-02-04", "dajamo32@hotmail.com");
    when(pacienteDAO.getPaciente(anyString())).thenReturn(paciente);

    mockMvc
      .perform(get("/paciente/11111111W"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(6)))
      .andExpect(jsonPath("$.dniPaciente", is(paciente.getDniPaciente())))
      .andExpect(jsonPath("$.nombrePaciente", is(paciente.getNombrePaciente())))
      .andExpect(jsonPath("$.apellidosPaciente", is(paciente.getApellidosPaciente())))
      .andExpect(jsonPath("$.gender", is(paciente.getGender())))
      .andExpect(jsonPath("$.birthday", is(paciente.getBirthday())))
      .andExpect(jsonPath("$.email", is(paciente.getEmail())));
  }
  @Test
  public void getPacienteApellidos_llamarConPacienteExistente_devuelveOk()
    throws Exception {
    Paciente paciente = new Paciente("11111111W", "David","Vinas Viruta", "Hombre", "2010-02-04", "dajamo32@hotmail.com");
    when(pacienteDAO.getPaciente(anyString())).thenReturn(paciente);

    mockMvc
      .perform(get("/paciente/11111111W"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(6)))
      .andExpect(jsonPath("$.dniPaciente", is(paciente.getDniPaciente())))
      .andExpect(jsonPath("$.nombrePaciente", is(paciente.getNombrePaciente())))
      .andExpect(jsonPath("$.apellidosPaciente", is(paciente.getApellidosPaciente())))
      .andExpect(jsonPath("$.gender", is(paciente.getGender())))
      .andExpect(jsonPath("$.birthday", is(paciente.getBirthday())))
      .andExpect(jsonPath("$.email", is(paciente.getEmail())));
  }*/

  @Test
  public void addPaciente_whenWorks() throws Exception {
    Paciente paciente = new Paciente("12345678A", "San", "The Man", "X", "1991-07-04", "correo@correo.es");

    mockMvc.perform(post("/nuevoPaciente")
      .content(new JSONObject(paciente).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(content().string(""));
  }

  @Test
  public void getPacientesPagination_llamarConPacienteExistente_devuelveOk() throws Exception {
   Paciente paciente = new Paciente("11111111W", "David","Vinas Viruta", "Hombre", "2010-02-04", "dajamo32@hotmail.com");
   InfoPage info = new InfoPage (1, 1, 1);
    List<Paciente> pacientes = List.of(paciente);
    ListPacienteResponse listPacienteResponse = new ListPacienteResponse(pacientes,info);

   when(baseDAO.getPageWithTipo(info,"Paciente",Paciente.class)).thenReturn(pacientes);
   when(pacienteResponseFactory.newPacienteResponse(any(),any())).thenReturn(listPacienteResponse);

    mockMvc
            .perform(get("/getPacientesPaginacion/Pagina?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.pacientes", hasSize(1)))
            .andExpect(jsonPath("$.infoPagina.totalPages",is(1)));
  }
}
