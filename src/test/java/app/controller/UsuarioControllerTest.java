package app.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import app.dao.BaseDAO;
import app.factory.UsuarioResponseFactory;
import app.model.Paciente;
import app.model.response.InfoPage;
import app.model.response.ListPacienteResponse;
import app.model.response.ListUsuarioResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import app.dao.UsuarioDAO;
import app.model.Usuario;

import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {
  
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UsuarioDAO usuarioDAO;

  @MockBean
  BaseDAO baseDAO;

  @MockBean
  UsuarioResponseFactory usuarioResponseFactory;

  @Test
  public void getUsuario_llamarConUsuarioInexistente_devuelveUnauthorized()
    throws Exception {
    when(usuarioDAO.getUsuario(anyString())).thenReturn(null);

    mockMvc.perform(get("/usuario/notExisting/ ")).andExpect(status().isUnauthorized());
  }


  @Test
  public void getUsuario_llamarConUsuarioExistenteYPasswordOk_devuelveOk()
    throws Exception {
    Usuario usuario = new Usuario("Dummy Perez","Dummy Apellido", "12345678A","Dummy", "123456");
    when(usuarioDAO.getUsuario(anyString())).thenReturn(usuario);

    mockMvc
      .perform(get("/usuario/12345678A/123456"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(5)))
      .andExpect(jsonPath("$.nombreDoctor", is(usuario.getNombreDoctor())))
      .andExpect(jsonPath("$.dniDoctor", is(usuario.getDniDoctor())))
      .andExpect(jsonPath("$.nombreUsuario", is(usuario.getNombreUsuario())))
      .andExpect(jsonPath("$.password", is(usuario.getPassword())));
  }

  @Test
  public void getUsuario_llamarConUsuarioExistenteYPasswordNotOk_devuelveNotFound()
    throws Exception {
    Usuario usuario = new Usuario("Dummy Perez","Dummy Apellido", "12345678A", "Dummy", "123456");
    when(usuarioDAO.getUsuario(anyString())).thenReturn(usuario);

    mockMvc.perform(get("/usuario/12345678A/12347")).andExpect(status().isUnauthorized());
  }

  @Test
  public void getEmployees_when_callled_devuelveOk() throws Exception {
    Usuario usuario = new Usuario("dummyNombre","Dummy Apellido","dummyDni","dummyNombreUsuario","dummyContraseña");
    InfoPage info = new InfoPage (1, 1, 1);
    List<Usuario> usuarios = List.of(usuario);
    ListUsuarioResponse listUsuarioResponse = new ListUsuarioResponse(usuarios,info);

    when(baseDAO.getPageWithTipo(info,"dummyTabla",Usuario.class)).thenReturn(usuarios);
    when(usuarioResponseFactory.newUsuarioResponse(any(),any())).thenReturn(listUsuarioResponse);

    mockMvc
            .perform(get("/getEmployeesPaginacion/Pagina?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.usuarios", hasSize(1)))
            .andExpect(jsonPath("$.infoPagina.totalPages",is(1)));
  }
}
